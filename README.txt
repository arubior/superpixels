Modification of David Stutz's SEEDS Revised algorithm for superpixel segmentation of images [1].

# 1. Compile:

In root directory:

$ mkdir -p build
$ cd build
$ cmake ..
$ make

# 2. Utilization:

In root directory (optional parameters in brackets):

$ ./bin/cli /input_images_directory --output /output_directory (--csv) (--contour) (--mean) (--superpixels 50)

-------------------------------------------
[1] David Stutz, Alexander Hermans and Bastian Leibe. Superpixel Segmentation using Depth Information. September 2014,RWTH Aachen University (Aachen, Germany) http://davidstutz.de/
